<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sample' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'zbcnDmvbh}Ca1$(C:()%8.j1i3xHss`iMKFu5ak=NH~d1cMl0@M<%%o}FJMU|8E8' );
define( 'SECURE_AUTH_KEY',  ']apS}8{39p-]2N5{]}kI(F<q);9YS}Jof?j-~ftXB]#Y3{9JxVb4#:>$GtmsRy{~' );
define( 'LOGGED_IN_KEY',    'B@fhSI#/=YAxkoz;_D?[4cHQ.e&?[ei-_78usCzl|1W:H$aVKFu9)[Qe)J?Gvml9' );
define( 'NONCE_KEY',        '$ Z/Q@n b<`In4|qh|gStuI^*E&+&Xv,l4MKq-Dpoki+uqfu!sRZeMoW,?l03)vl' );
define( 'AUTH_SALT',        'L*hD*Y%dZ!Oh)+(jWfhd$m_+<z)>9]ubUcjkGvUS3G|Fa,GO=mwPfb42m}mOtH]-' );
define( 'SECURE_AUTH_SALT', 'HgoTd#rpgeK1,c5}OCIs^8JKyzw3Tnu:8X}T~ls_0VE5/%?ppO?}.HU@QxRc@oJ|' );
define( 'LOGGED_IN_SALT',   'WN`a^@FaH}[,A`-d}4(D7fE[VRwH4X?lO~B~z-.5.0hZwX26gmJ*Pt8^`@{*CMlh' );
define( 'NONCE_SALT',       '7XM~:|X-5L+o8}d;7TpbTZG<lw?:B9Z^pRA6sRa(QF)X_aBw,P)Y8;F-G/$7FEq>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
